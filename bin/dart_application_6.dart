import 'dart:io';
//import 'dart:js_util';

class sum {
  // defining a class
  var n1;
  var n2;

  int calSum(int num1, int num2) {
    int sum2num = num1 + num2;
    return sum2num;
  }
}

class Subtraction {
  // defining a class
  var n1;
  var n2;

  int calSubtract(int num1, int num2) {
    int subt2num = num1 - num2;
    return subt2num;
  }
}

class multiply {
  // defining a class
  var n1;
  var n2;

  int calMultiply(int num1, int num2) {
    int mult2num = num1 * num2;
    return mult2num;
  }
}

class divide {
  // defining a class
  var n1;
  var n2;

  double calDivide(int num1, int num2) {
    double div2num = num1 / num2;
    return div2num;
  }
}

class mod {
  // defining a class
  var n1;
  var n2;

  int calMod(int num1, int num2) {
    int mod2num = num1 % num2;
    return mod2num;
  }
}

void main() {
  // declaring an object s1, s2 of the class sum
  var s1 = sum();
  var s2 = sum();
  // declaring an object sub1, sub2 of the class Subtraction
  var sub1 = Subtraction();
  var sub2 = Subtraction();
  // declaring an object mul1, mul2 of the class Multiply
  var mul1 = multiply();
  var mul2 = multiply();

  // declaring an object div1, div2 of the class Divide
  var div1 = divide();
  var div2 = divide();

  // declaring an object m1, m2 of the class Mod
  var m1 = mod();
  var m2 = mod();

  print("Please Input value position 1");
  var num1 = stdin.readLineSync();
  print("Please Input Operator");
  var operand = stdin.readLineSync();
  print("Please Input value position 2");
  var num2 = stdin.readLineSync();
  var inum1 = int.parse(num1!);
  var inum2 = int.parse(num2!);
  // calling the method of the class. Variable a is used to hold the value returned by the function
  switch (operand) {
    case '+':
      s1.n1 = inum1;
      s2.n2 = inum2;

      int sum = s1.calSum(s1.n1, s2.n2);
      print(" \n Answer sum of $inum1 + $inum2 is : $sum ");
      break;

    case '-':
      sub1.n1 = inum1;
      sub2.n2 = inum2;

      int subtraction = sub1.calSubtract(sub1.n1, sub2.n2);
      print(" \n Answer subtraction of $inum1 - $inum2 is : $subtraction ");
      break;

    case '*':
      mul1.n1 = inum1;
      mul2.n2 = inum2;

      int multiply = mul1.calMultiply(mul1.n1, mul2.n2);
      print(" \n Answer multiply of $inum1 * $inum2 is : $multiply ");
      break;

    case '/':
      div1.n1 = inum1;
      div2.n2 = inum2;

      double divide = div1.calDivide(div1.n1, div2.n2);
      print(" \n Answer divide of $inum1 / $inum2 is : $divide ");
      break;

    case '%':
      m1.n1 = inum1;
      m2.n2 = inum2;

      int mod = m1.calMod(m1.n1, m2.n2);
      print(" \n Answer mod of $inum1 / $inum2 is : $mod ");
      break;
  }
}//Finish
